package com.mfu.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class orderItem implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long orderId;
	
	private String productName;
	private String quantity;
	public long getOrderId() {
		return orderId;
	}
	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	
	public void getTotalPrice() {
		
	}
	
	@ManyToOne
	private customerOrder cusOrder;
	public customerOrder getCusOrder() {
		return cusOrder;
	}
	public void setCusOrder(customerOrder cusOrder) {
		this.cusOrder = cusOrder;
	}

}
