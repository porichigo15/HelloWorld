package com.mfu.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
public class customerOrder implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long cusId;
	
	private Date date;

	public long getCusId() {
		return cusId;
	}

	public void setCusId(long cusId) {
		this.cusId = cusId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public void getTotalPrice() {
		
	}
	
	@OneToMany(mappedBy="cusOrder", cascade={CascadeType.ALL})
	private List<orderItem> orderitem;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Car car;

	public List<orderItem> getOrderitem() {
		return orderitem;
	}

	public void setOrderitem(List<orderItem> orderitem) {
		this.orderitem = orderitem;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}
	
	
}
