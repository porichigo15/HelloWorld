package com.mfu.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

@Entity
public class Car implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long carId;
	
	private String username;
	private String password;
	private String firstname;
	private String lastname;
	private String email;
	private String phone;
	public long getCarId() {
		return carId;
	}
	public void setCarId(long carId) {
		this.carId = carId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}

	@OneToMany(mappedBy="car", cascade={CascadeType.ALL})
	private List<customerOrder> cusOrder;
	public List<customerOrder> getCusOrder() {
		return cusOrder;
	}
	public void setCusOrder(List<customerOrder> cusOrder) {
		this.cusOrder = cusOrder;
	}
	
	
	
}
