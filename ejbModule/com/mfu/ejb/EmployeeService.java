package com.mfu.ejb;

import java.util.List;

import com.mfu.entity.Employee;

public interface EmployeeService {

	public void insert(Employee emp);
	public Employee findEmployee(long empId);
	public void update(Employee emp);
	public void delete(long empId);
	public List<Employee> getAllEmployee();
	public List<Employee> findEmployeeFirstname(String name);
}