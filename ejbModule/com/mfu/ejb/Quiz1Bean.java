package com.mfu.ejb;

import javax.ejb.Remote;
import javax.ejb.Stateless;

@Stateless
@Remote(Quiz1.class)
public class Quiz1Bean {

	public Quiz1Bean() {
		
	}
	
	public int plus(int a, int b) {
		int result = 0;
		result = a + b;
		return result;
	}
	
	public int minus(int a, int b) {
		int result = 0;
		result = a - b;
		return result;
	}
	
	public int multiply(int a, int b) {
		int result = 0;
		result = a * b;
		return result;
	}
}
