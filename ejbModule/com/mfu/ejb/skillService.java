package com.mfu.ejb;

import java.util.List;

import com.mfu.entity.Skill;

public interface skillService {

	public void insert(Skill ph);
	public Skill findSkill(long sId);
	public void update(Skill ph);
	public void deleteSkill(long phId);
	public List<Skill> getAllSkill();
	public List<Skill> findSkill(String skill);
	public List<Skill> findSkillByEmployee(long empId);
}
