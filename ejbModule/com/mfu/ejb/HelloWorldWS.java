package com.mfu.ejb;

import javax.jws.WebService;

@WebService
public interface HelloWorldWS {

	public String sayHello(String name);
}
