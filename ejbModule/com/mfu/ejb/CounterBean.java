package com.mfu.ejb;

import javax.ejb.Remote;
import javax.ejb.Stateful;

@Stateful
//Stateful = send request back when method call
@Remote(Counter.class)
public class CounterBean implements Counter{
	
	public CounterBean() {
		
	}
	
	private int count = 0;
	
	@Override
	public void increment() {
		this.count++;
	}
	
	@Override
	public void decrement() {
		this.count--;
	}
	
	@Override
	public int getCount() {
		return this.count;
	}
}
