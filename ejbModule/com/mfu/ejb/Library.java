package com.mfu.ejb;

import java.util.List;

import com.mfu.entity.Book;

public interface Library {
	
	public void addBook(String name);
	
	public List<Book> listBook();
}
