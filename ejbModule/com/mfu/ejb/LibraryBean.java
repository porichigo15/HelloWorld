package com.mfu.ejb;


import java.util.*;

import javax.ejb.Remote;
import javax.ejb.Stateful;

import com.mfu.entity.Book;

@Stateful
//Stateful = send request back when method call
@Remote(Library.class)
public class LibraryBean {
	
	public List<Book> listbook = new ArrayList<Book>();
	public LibraryBean() {
	}
	
	public void addBook(String name) {
		Book newBook = new Book();
		newBook.setName(name);
		listbook.add(newBook);
	}
	
	public List<Book> listBook(){
		return listbook;
		
	}
}
