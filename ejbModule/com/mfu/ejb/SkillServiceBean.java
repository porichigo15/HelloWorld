package com.mfu.ejb;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.mfu.entity.Skill;

@Stateless
@Remote(skillService.class)
public class SkillServiceBean implements skillService{
	
	@PersistenceContext(unitName = "employeeDatabase")
	EntityManager em;
	
	public void insert(Skill ph) {
		this.em.persist(ph);
	}
	
	public Skill findSkill(long sId) {
		return this.em.find(Skill.class, sId);
	}
	
	public void update(Skill ph) {
		this.em.merge(ph);

		}
	
	public void deleteSkill(long phId) {
		Skill ph = findSkill(phId);
		if(ph != null) {
			em.remove(ph);
		}
		
	}
	
	public List<Skill> getAllSkill() {
		return em.createQuery("SELECT x FROM Skill x").getResultList();
		}
	
	public List<Skill> findSkill(String skill) {
		return em.createQuery("SELECT em FROM Skill em WHERE em.skill LIKE :fn ").setParameter("fn", skill + "%").getResultList();
		}
	
	public List<Skill> findSkillByEmployee(long empId) {
		return em.createQuery("SELECT ph FROM Skill ph WHERE ph.employee.employeeId=:param1").setParameter("param1",empId).getResultList();
		}
}
