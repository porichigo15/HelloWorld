package com.mfu.ejb;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.mfu.entity.Employee;
import com.mfu.entity.Phone;

@Stateless
@Remote(phoneService.class)
public class phoneServiceBean implements phoneService{
	
	@PersistenceContext(unitName = "phoneDatabase")
	EntityManager em;
	
	public void insert(Phone ph) {
		this.em.persist(ph);
	}
	
	public Phone findPhone(long phId) {
		return this.em.find(Phone.class, phId);
	}
	
	public void update(Phone ph) {
		this.em.merge(ph);

		}
	
	public void deletePhone(long phId) {
		Phone ph = findPhone(phId);
		if(ph != null) {
			em.remove(ph);
		}
		
	}
	
	public List<Phone> getAllPhone() {
		return em.createQuery("SELECT x FROM Phone x").getResultList();
		}
	
	public List<Phone> findPhoneLabel(String label) {
		return em.createQuery("SELECT em FROM Phone em WHERE em.label LIKE :fn ").setParameter("fn", label + "%")
		.getResultList();
		}
	
	public List<Phone> findPhoneByEmployee(long empId) {
		return em.createQuery("SELECT ph FROM Phone ph WHERE ph.employee.employeeId=:param1").setParameter("param1",empId).getResultList();
		}
}
