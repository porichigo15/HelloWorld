package com.mfu.ejb;

public interface Counter {
	
	public void increment();
	
	public void decrement();
	
	public int getCount();
}
