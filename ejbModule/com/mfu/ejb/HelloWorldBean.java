package com.mfu.ejb;

import java.util.logging.Logger;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.jws.WebService;

@Stateless
@Remote(HelloWorld.class)
@WebService(serviceName="HelloWorldWS", targetNamespace="http://www.mfu.com/ejb3/hello")
public class HelloWorldBean implements HelloWorld, HelloWorldWS{

	Logger logger = null;

	public HelloWorldBean() {
		//logger = Logger.getLogger(getClass()); 
	}
	@Override
	public String sayHello(String name) {
		String msg = "Hello "+name;
		System.out.println(msg);
		return msg;
	}
	
}
