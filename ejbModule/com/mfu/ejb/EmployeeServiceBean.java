package com.mfu.ejb;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.mfu.entity.Employee;

@Stateless
@Remote(EmployeeService.class)
public class EmployeeServiceBean implements EmployeeService{
	
	@PersistenceContext(unitName = "employeeDatabase")
	EntityManager em;
	
	public void insert(Employee emp) {
		this.em.persist(emp);
	}
	
	public Employee findEmployee(long empId) {
		return this.em.find(Employee.class, empId);
	}
	
	public void update(Employee emp) {
		this.em.merge(emp);

		}
	
	public void delete(long empId) {
		Employee emp = findEmployee(empId);
		if(emp != null) {
			em.remove(emp);
		}
	}
	
	public List<Employee> getAllEmployee() {
		return em.createQuery("SELECT x FROM Employee x").getResultList();
		}
	
	public List<Employee> findEmployeeFirstname(String name) {
		return em.createQuery("SELECT em FROM Employee em WHERE em.firstName LIKE :fn ").setParameter("fn", name + "%")
		.getResultList();
		}
}
