package com.mfu.ejb;

import java.util.List;

import com.mfu.entity.Phone;

public interface phoneService {

	public void insert(Phone ph);
	public Phone findPhone(long phId);
	public void update(Phone ph);
	public void deletePhone(long phId);
	public List<Phone> getAllPhone();
	public List<Phone> findPhoneLabel(String label);
	public List<Phone> findPhoneByEmployee(long empId);
}
