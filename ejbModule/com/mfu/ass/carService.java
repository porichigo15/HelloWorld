package com.mfu.ass;

import java.util.List;

import com.mfu.entity.Car;

public interface carService {
	
	public void insert(Car car);
	public Car findCar(long carId);
	public void update(Car car);
	public void delete(long carId);
	public List<Car> getAllCar();
	public List<Car> findCarFirstname(String fname);
}
