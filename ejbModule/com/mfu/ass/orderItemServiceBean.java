package com.mfu.ass;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.mfu.entity.*;

@Stateless
@Remote(orderItemService.class)
public class orderItemServiceBean implements orderItemService{
	
	@PersistenceContext(unitName = "employeeDatabase")
	EntityManager em;
	
	public void insert(orderItem item) {
		this.em.persist(item);
	}
	
	public orderItem findorderItem(long itemId) {
		return this.em.find(orderItem.class, itemId);
	}
	
	public void update(orderItem item) {
		this.em.merge(item);

		}
	
	public void deleteorderItem(long cusId, long itemId) {
		customerOrder cus = em.find(customerOrder.class, cusId);
		for(int i = 0; i < cus.getOrderitem().size(); i++) {
			orderItem item = cus.getOrderitem().get(i);
			if(item.getOrderId() == itemId) {
				cus.getOrderitem().remove(i);
				em.remove(item);
			}
		}
		
		em.merge(cus);
		
	}
	
	public List<orderItem> getAllorderItem() {
		return em.createQuery("SELECT x FROM orderItem x").getResultList();
		}
	
	public List<orderItem> findorderItemProdName(String name) {
		return em.createQuery("SELECT em FROM orderItem em WHERE em.productName LIKE :fn ").setParameter("fn", name + "%")
		.getResultList();
		}
}
