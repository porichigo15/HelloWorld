package com.mfu.ass;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.mfu.entity.*;

@Stateless
@Remote(customerService.class)
public class customerServiceBean implements customerService{
	
	@PersistenceContext(unitName = "customerDatabase")
	EntityManager em;
	
	public void insert(customerOrder cus) {
		this.em.persist(cus);
	}
	
	public customerOrder findcustomerOrder(long cusId) {
		return this.em.find(customerOrder.class, cusId);
	}
	
	public void update(customerOrder cus) {
		this.em.merge(cus);

		}
	
	public void deletecustomerOrder(long carId, long cusId) {
		Car car = em.find(Car.class, carId);
		for(int i = 0; i < car.getCusOrder().size(); i++) {
			customerOrder cus = car.getCusOrder().get(i);
			if(cus.getCusId() == cusId) {
				car.getCusOrder().remove(i);
				em.remove(cus);
			}
		}
		
		em.merge(car);
		
	}
	
	public List<customerOrder> getAllcustomerOrder() {
		return em.createQuery("SELECT x FROM customerOrder x").getResultList();
		}
	
	public List<customerOrder> findcustomerOrderDate(Date date) {
		return em.createQuery("SELECT em FROM customerOrder em WHERE em.date LIKE :fn ").setParameter("fn", date + "%")
		.getResultList();
		}


}
