package com.mfu.ass;

import java.util.List;

import com.mfu.entity.orderItem;

public interface orderItemService {

	public void insert(orderItem item);
	public orderItem findorderItem(long itemId);
	public void update(orderItem item);
	public void deleteorderItem(long cusId, long itemId);
	public List<orderItem> getAllorderItem();
	public List<orderItem> findorderItemProdName(String name);
}
