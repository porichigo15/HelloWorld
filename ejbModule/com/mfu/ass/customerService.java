package com.mfu.ass;

import java.util.Date;
import java.util.List;

import com.mfu.entity.customerOrder;
import com.sun.jmx.snmp.Timestamp;

public interface customerService {
	
	public void insert(customerOrder cus);
	public customerOrder findcustomerOrder(long cusId);
	public void update(customerOrder cus);
	public void deletecustomerOrder(long carId, long cusId);
	public List<customerOrder> getAllcustomerOrder();
	public List<customerOrder> findcustomerOrderDate(Date date);
}
