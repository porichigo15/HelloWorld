package com.mfu.ass;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.mfu.entity.*;

@Stateless
@Remote(carService.class)
public class carServiceBean implements carService{
	
	@PersistenceContext(unitName = "carDatabase")
	EntityManager em;
	
	public void insert(Car car) {
		this.em.persist(car);
	}
	
	public Car findCar(long carId) {
		return this.em.find(Car.class, carId);
	}
	
	public void update(Car car) {
		this.em.merge(car);

		}
	
	public void delete(long carId) {
		Car car = findCar(carId);
		if(car != null) {
			em.remove(car);
		}
	}
	
	public List<Car> getAllCar() {
		return em.createQuery("SELECT x FROM Car x").getResultList();
		}
	
	public List<Car> findCarFirstname(String fname) {
		return em.createQuery("SELECT em FROM Car em WHERE em.firstname LIKE :fn ").setParameter("fn", fname + "%")
		.getResultList();
		}
}
